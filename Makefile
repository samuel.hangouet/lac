SANITIZE = -D_GLIBCXX_DEBUG # -fsanitize=address,undefined -fno-sanitize-recover=address,undefined

chess: main.cpp $(wildcard *.h) Makefile
	clang++-15 -std=c++20 -Wall -O3 -o $@ $< -Iexternals -DNDEBUG

chess.debug: main.cpp $(wildcard *.h) Makefile
	clang++-15 -std=c++20 -Wall -g -o $@ $< -Iexternals $(SANITIZE)

chess.test: test.cpp $(wildcard *.h) Makefile
	clang++-15 -std=c++20 -Wall -g -o $@ $< -Iexternals $(SANITIZE)

test: chess.test
	./$<

run: chess
	./$<

.SUFFIXES:
.SECONDARY:
