#include "Math.h"
#include <vector>
#include <cassert>
#include <array>
#include <iostream>

enum Pieces { Pawn=1, Knight=3, Rook=5, Queen=9, King=10 };

inline char get_piece_name(int8_t p)
{
    switch(p)
    {
        case Pawn: return 'P';
        case Rook: return 'R';
        case Knight: return 'N';
        case Queen: return 'Q';
        case King: return 'K';
        case 0: return '_';
        case -Pawn: return 'p';
        case -Rook: return 'r';
        case -Knight: return 'n';
        case -Queen: return 'q';
        case -King: return 'k';
    }
    return '?';
}

struct Move
{
    int8_t from;
    int8_t to;
    int8_t taken;
    int8_t promotion;

    friend std::ostream & operator << (std::ostream & os, const Move & m)
    {
        auto fx = int8_t('a' + m.from % 6);
        auto fy = int8_t('1' + m.from / 6);
        auto tx = int8_t('a' + m.to % 6);
        auto ty = int8_t('1' + m.to / 6);
        os << fx << fy;
        os << (m.taken != 0 ? 'x' : '-');
        os << tx << ty;
        if (m.promotion != 0)
        {
            os << ':' << get_piece_name(m.promotion);
        }
        return os;
    }
};

struct Board
{
    int trait = 1;

    std::array<int8_t, 6*6> content;

    int8_t whiteKingX = -1;
    int8_t whiteKingY = -1;
    int8_t blackKingX = -1;
    int8_t blackKingY = -1;

    std::vector<int8_t> fiftyMovesCount = { 0 };

    static constexpr auto index(int x, int y) { return int8_t(y * 6 + x); }

    int8_t & at(int x, int y) { return content[index(x, y)]; }
    int8_t at(int x, int y) const { return content[index(x, y)]; }

    friend std::ostream & operator << (std::ostream & os, const Board & b)
    {
        if (b.trait == 1)
            os << "White to play\n";
        else
            os << "Black to play\n";
        for (int j = 5; j >= 0; --j)
        {
            os << (j + 1) << ' ';
            for (int i = 0; i <= 5; ++i)
            {
                os << ' ' << get_piece_name(b.at(i, j));
            }
            os << '\n';
        }
        os << "   a b c d e f\n";
        return os;
    }

    Board()
    {
        content.fill(0);

        at(0, 0) = Rook;
        at(1, 0) = Knight;
        placeKing(King, 2, 0);
        at(3, 0) = Queen;
        at(4, 0) = Knight;
        at(5, 0) = Rook;

        for (int i = 0; i < 6; ++i)
        {
            at(i, 1) = Pawn;
            at(i, 4) = -Pawn;
        }

        at(0, 5) = -Rook;
        at(1, 5) = -Knight;
        at(2, 5) = -King;
        placeKing(-King, 2, 5);
        at(3, 5) = -Queen;
        at(4, 5) = -Knight;
        at(5, 5) = -Rook;

        trait = 1;
    }

    void placeKing(int8_t king, int8_t x, int8_t y)
    {
        at(x, y) = king;
        if (king > 0)
        {
            whiteKingX = x;
            whiteKingY = y;
        }
        else
        {
            blackKingX = x;
            blackKingY = y;
        }
    }

    void apply(const Move & move)
    {
        auto p = content[move.from];
        assert(p * trait > 0);
        assert(content[move.to] == move.taken);
        assert(move.taken * trait <= 0);

        if (p * trait == Pawn or move.taken != 0)
            fiftyMovesCount.push_back(0);
        else
            fiftyMovesCount.push_back(fiftyMovesCount.back() + 1);

        content[move.from] = 0;
        if (move.promotion)
        {
            content[move.to] = move.promotion;
        }
        else
        {
            if (p == King)
            {
                whiteKingX = move.to % 6;
                whiteKingY = move.to / 6;
            }
            else if (p == -King)
            {
                blackKingX = move.to % 6;
                blackKingY = move.to / 6;
            }
            content[move.to] = p;
        }
        trait = -trait;
    }

    void unapply(const Move & move)
    {
        trait = -trait;

        auto p = content[move.to];
        if (move.promotion)
        {
            content[move.from] = trait * Pawn;
        }
        else
        {
            if (p == King)
            {
                whiteKingX = move.from % 6;
                whiteKingY = move.from / 6;
            }
            else if (p == -King)
            {
                blackKingX = move.from % 6;
                blackKingY = move.from / 6;
            }
            content[move.from] = p;
        }
        content[move.to] = move.taken;

        fiftyMovesCount.pop_back();
    }

    bool checkCheck(int color)
    {
        int x, y;
        if (color > 0)
        {
            x = whiteKingX;
            y = whiteKingY;
        }
        else
        {
            x = blackKingX;
            y = blackKingY;
        }

        bool check = false;
        for (int8_t p: {Queen, Rook, Pawn, Knight, King})
        {
            iterMovesFrom(color * p, x, y, [&](const Move & move, bool) {
                if (move.taken == -color * p)
                    check = true;
            });
        }
        return check;
    }

    // Iterates on all allowed moves
    template <typename C>
    int iterMoves(C callback)
    {
        int material = 0;
        for (int j = 0; j < 6; ++j)
        {
            for (int i = 0; i < 6; ++i)
            {
                // Also includes invalid moves:
                //   - leading to a check situation
                //   - whose taken piece has the same color
                int8_t piece = at(i, j);
                material += piece;
                iterMovesFrom(piece, i, j, [&](const Move & move, bool valid){
                    valid = valid and piece * trait > 0 and fiftyMovesCount.back() < 50;
                    if (valid)
                    {
                        // Check the fifty moves rule
                        // Also check for check
                        apply(move);
                        valid = ! checkCheck(-trait);
                        unapply(move);
                    }

                    callback(piece, move, valid);
                });
            }
        }
        return material;
    }

private:
    template <typename C>
    void iterMovesFrom(int8_t piece, int x, int y, C callback)
    {
        int color = sign(piece);

        switch (piece * color)
        {
        case Pawn:
            if (y == ((color > 0) ? 4 : 1))
            {
                if (at(x, y + color) == 0)
                {
                    callback({index(x, y), index(x, y + color), 0, Rook}, true);
                    callback({index(x, y), index(x, y + color), 0, Knight}, true);
                    callback({index(x, y), index(x, y + color), 0, Queen}, true);
                }
                if (x > 0)
                {
                    int8_t taken = at(x - 1, y + color);
                    bool valid = taken * color < 0;
                    callback({index(x, y), index(x - 1, y + color), taken, Rook}, valid);
                    callback({index(x, y), index(x - 1, y + color), taken, Knight}, valid);
                    callback({index(x, y), index(x - 1, y + color), taken, Queen}, valid);
                }
                if (x < 5)
                {
                    int8_t taken = at(x + 1, y + color);
                    bool valid = taken * color < 0;
                    callback({index(x, y), index(x + 1, y + color), taken, Rook}, valid);
                    callback({index(x, y), index(x + 1, y + color), taken, Knight}, valid);
                    callback({index(x, y), index(x + 1, y + color), taken, Queen}, valid);
                }
            }
            else
            {
                // Allow pawns to be on promotion row (usefull for checking for check)
                int ty = y + color;
                if (ty >= 0 and ty < 6)
                {
                    if (at(x, ty) == 0)
                        callback({index(x, y), index(x, ty), 0, 0}, true);

                    if (x > 0)
                    {
                        int8_t taken = at(x - 1, ty);
                        bool valid = taken * color < 0;
                        callback({index(x, y), index(x - 1, ty), taken, 0}, valid);
                    }
                    if (x < 5)
                    {
                        int8_t taken = at(x + 1, ty);
                        bool valid = taken * color < 0;
                        callback({index(x, y), index(x + 1, ty), taken, 0}, valid);
                    }
                }
            }
            break;
        case Queen:
            iterStraightMoves<-1, -1>(piece, x, y, callback);
            iterStraightMoves<+1, +1>(piece, x, y, callback);
            iterStraightMoves<-1, +1>(piece, x, y, callback);
            iterStraightMoves<+1, -1>(piece, x, y, callback);
            [[fallthrough]];
        case Rook:
            iterStraightMoves< 0, -1>(piece, x, y, callback);
            iterStraightMoves< 0, +1>(piece, x, y, callback);
            iterStraightMoves<-1,  0>(piece, x, y, callback);
            iterStraightMoves<+1,  0>(piece, x, y, callback);
            break;
        case King:
            iterSimpleMove<-1, -1>(piece, x, y, callback);
            iterSimpleMove<+1, +1>(piece, x, y, callback);
            iterSimpleMove<-1, +1>(piece, x, y, callback);
            iterSimpleMove<+1, -1>(piece, x, y, callback);
            iterSimpleMove< 0, -1>(piece, x, y, callback);
            iterSimpleMove< 0, +1>(piece, x, y, callback);
            iterSimpleMove<-1,  0>(piece, x, y, callback);
            iterSimpleMove<+1,  0>(piece, x, y, callback);
            break;
        case Knight:
            iterSimpleMove<-1, -2>(piece, x, y, callback);
            iterSimpleMove<-1,  2>(piece, x, y, callback);
            iterSimpleMove< 1,  2>(piece, x, y, callback);
            iterSimpleMove< 1, -2>(piece, x, y, callback);
            iterSimpleMove<-2,  1>(piece, x, y, callback);
            iterSimpleMove<-2, -1>(piece, x, y, callback);
            iterSimpleMove< 2,  1>(piece, x, y, callback);
            iterSimpleMove< 2, -1>(piece, x, y, callback);
            break;
        }
    }

    template <int DX, int DY>
    bool tryMove(int x, int y) const
    {
        if constexpr (DX < 0)
        {
            if (x + DX < 0)
                return false;
        }
        else if constexpr (DX > 0)
        {
            if (x + DX >= 6)
                return false;
        }

        if constexpr (DY < 0)
        {
            if (y + DY < 0)
                return false;
        }
        else if constexpr (DY > 0)
        {
            if (y + DY >= 6)
                return false;
        }

        return true;
    }

    template <int DX, int DY, typename C>
    void iterSimpleMove(int8_t piece, int x, int y, C callback)
    {
        if (not tryMove<DX, DY>(x, y))
            return;

        auto taken = at(x + DX, y + DY);
        bool valid = taken * piece <= 0;
        callback({index(x, y), index(x + DX, y + DY), taken, 0}, valid);
    }

    template <int DX, int DY, typename C>
    void iterStraightMoves(int8_t piece, int i, int j, C callback)
    {
        int x = i;
        int y = j;
        while (true)
        {
            if (not tryMove<DX, DY>(x, y))
                return;
            x += DX;
            y += DY;
            auto taken = at(x, y);
            bool valid = taken * piece <= 0;
            callback({index(i, j), index(x, y), taken, 0}, valid);
            if (taken != 0)
                return;
        }
    }
};

