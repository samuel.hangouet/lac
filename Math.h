#pragma once
inline constexpr int sign(int v) { return (v > 0) - (v < 0); }
