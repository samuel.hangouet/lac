#include "Game.h"

int main(int argc, const char** argv)
{
    int strength = 100;
    int seed = 0;
    Game game(seed, strength);
    game.loop();
}
