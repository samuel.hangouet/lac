#pragma once
#include <algorithm>
#include <cassert>
#include <random>
#include <iostream>
#include <limits>
#include <cstdint>
// Xor shift 128 plus random rengine
struct XorShift128plus
{
    typedef uint64_t result_type;

    uint64_t a;
    uint64_t b;
    
    template <typename C>
    void enumerate(C c)
    {
        c("a", a);
        c("b", b);
    }

    XorShift128plus(uint64_t seed = 0)
    { 
        this->seed(seed);
    }

    void seed(uint64_t seed)
    { 
        a = 1234567891234ul * (1 + seed);
        b = 9876543219876ul;
        (*this)();
        (*this)();
        (*this)();
        (*this)();
        (*this)();
        (*this)();
    }

    static constexpr uint64_t min() { return 0; }
    static constexpr uint64_t max() { return std::numeric_limits<uint64_t>::max(); }

    uint64_t operator() ()
    {
        uint64_t t = a;
        const uint64_t s = b;
        a = s;
        t ^= t << 23;
        t ^= t >> 17;
        t ^= s ^ (s >> 26);
        b = t;
        return t + s;
    }
};

struct Random
{
    XorShift128plus _engine;

    Random(int seed = 0) : _engine(size_t(seed)) {}

    template <typename T>
    T rand(T min, T max)
    {
        if constexpr (std::is_integral<T>())
        {
            return std::uniform_int_distribution<T>(min, max)(_engine);
        }
        else
            return std::uniform_real_distribution<T>(min, max)(_engine);
    }

    // Returns inside [0; 1)
    float rand() { return rand<float>(0, 1); }

    // Returns inside [0; size-1]
    int randIndex(int size)
    {
        if (size <= 0)
            return 0;
        return this->rand<int>(0, size - 1);
    }

    void setSeed(int seed) { _engine.seed(size_t(seed)); }

    template <typename C>
    auto & choose(C & c)
    {
        assert(! c.empty());
        return c[rand<int>(0, (int)c.size() - 1)];
    }

    template <typename IT>
    auto choose(IT begin, IT end)
    {
        int size = (int)(end - begin);
        assert(size > 0);
        return begin[rand<int>(0, size - 1)];
    }

    template <typename T>
    T normal(T mean = 0, T stddev = 1)
    {
        return std::normal_distribution<T>(mean, stddev)(_engine);
    }

    template <typename IT>
    void shuffle(IT begin, IT end)
    {
        std::shuffle(begin, end, _engine);
    }

    template <typename T>
    int select(const T *array, int count, T sum)
    {
        ASSERT(sum > 0);
        T acc = 0;
        T p = rand<T>(0, sum);
        for (int i = 0; i < count - 1; ++i)
        {
            T weight = array[i];
            if (weight <= 0)
                continue;
            acc += weight;
            if (p <= acc)
                return i;
        }
        return count - 1;
    }

    friend std::ostream & operator << (std::ostream & os, const Random & r)
    {
        os << "Random(a=" << r._engine.a << ", b=" << r._engine.b << ")";
        return os;
    }
};
