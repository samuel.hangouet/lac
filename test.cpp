#include "Game.h"
#include <cassert>

void test_init_eval_zero()
{
    Game game(0, 1);
    assert(game.root.nodeEval == 0);
}

void test_basic_mate()
{
    Board board;
    board.content.fill(0);
    board.at(0, 0) = Rook;
    board.placeKing(King, 3, 3);
    board.placeKing(-King, 5, 3);

    Game game(0, 50);
    game.white = Modes::Best;
    game.load(board);
    game.printAnalysis(std::cout, game.current, -1);

    std::ostringstream oss;
    oss << game.current->children[game.current->bestChild];
    std::string move = oss.str();
    assert(move.starts_with("a1-f1"));
    assert(game.loop() == 1);
}

void test_random_game()
{
    Game game(0, 100);
    game.white = Modes::Best;
    game.loop();
}

void test_longer_mate()
{
    Board board;
    board.content.fill(0);
    board.at(0, 0) = Rook;
    board.at(0, 5) = Rook;
    board.placeKing(King, 1, 3);
    board.placeKing(-King, 5, 3);

    Game game(0, 100);
    game.white = Modes::Best;
    game.load(board);
    assert(game.loop() == 1);
}

int main()
{
    //test_init_eval_zero();
    //test_basic_mate();
    //test_random_game();
    test_longer_mate();
    return 0;
}

