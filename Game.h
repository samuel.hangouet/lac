#include "Board.h"
#include "Random.h"
#include "Math.h"
#include <algorithm>
#include <vector>
#include <cassert>
#include <limits>
#include <sstream>
#include <string>

enum class Modes
{
    Best,
    Random,
    Human
};

struct Node
{
    std::vector<Node> children;
    Node * parent = nullptr;
    Move move = {0,0,0,0};
    int count = 0;
    int nodeEval = 0;
    int treeEval = 0;
    int bestChild = -1;
    bool check = false;

    friend std::ostream & operator << (std::ostream & os, const Node & n)
    {
        if (n.parent == nullptr)
            os << "root";
        else
            os << n.move;
        if (n.children.empty())
        {
            if (n.check)
                os << '#';
            else
                os << '~';
        }
        else if (n.check)
        {
            os << '+';
        }
        int exploredChildren = 0;
        for (auto & child: n.children)
        {
            if (child.count > 0)
                exploredChildren ++;
        }
        os << " node=" << n.nodeEval
           << " tree=" << n.treeEval << "/" << n.count
           << " exploredChildren=" << exploredChildren << "/" << n.children.size();
        return os;
    }
};

struct Game
{
    static constexpr int Mate = std::numeric_limits<int>::max();

    Modes white = Modes::Human;
    Modes black = Modes::Best;
    Board board;
    Node root;
    Random re;
    Node * current;
    int strength;

    Game(int seed, int strength)
      : re(seed),
        strength(strength)
    { 
        current = &root;
        analyze();
    }

    int loop()
    {
        std::cout << board << '\n';
        while (true)
        {
            if (current->children.empty())
            {
                printHistory(std::cout, current);

                if (board.fiftyMovesCount.back() < 50 and current->check)
                {
                    std::cout << "Mate.\n";
                    if (board.trait == 1)
                    {
                        std::cout << "Black won!\n";
                        return -1;
                    }
                    else
                    {
                        std::cout << "White won!\n";
                        return 1;
                    }
                }

                std::cout << "Draw.\n";
                return 0;
            }


            Node * choice = nullptr;
            auto policy = board.trait == 1 ? white : black;

            if (policy == Modes::Random)
            {
                choice = & re.choose(current->children);
            }
            else if (policy == Modes::Best)
            {
                choice = & current->children[current->bestChild];
            }
            else
            {
                assert(policy == Modes::Human);
                std::string cmd;
                std::cout << "> ";
                std::getline(std::cin, cmd);
     
                if (std::cin.eof() or cmd == "q")
                    return 0;

                if (cmd == "")
                {
                    std::cout << board << '\n';
                    continue;
                }

                if (cmd == "u" and current->parent != nullptr)
                {
                    unplay();
                    if (white != black and current->parent != nullptr)
                    {
                        unplay();
                    }

                    std::cout << board << '\n';
                    continue;
                }

                if (cmd == "?")
                {
                    printAnalysis(std::cout, current, 1);
                    continue;
                }

                if (cmd == "??")
                {
                    printAnalysis(std::cout, current, -1);
                    continue;
                }

                if (cmd == "h")
                {
                    printBestMove(std::cout, current);
                    continue;
                }

                for (auto & node: current->children)
                {
                    std::ostringstream oss;
                    oss << node.move;
                    if (oss.str().starts_with(cmd))
                    {
                        choice = &node;
                        break;
                    }
                }

                if (choice == nullptr)
                {
                    std::cout << "Invalid move\n";
                    continue;
                }
            }

            std::cout << "Play " << get_piece_name(board.content[choice->move.from]) << choice->move << '\n';
            play(choice);
            std::cout << board << '\n';
            if (current->check)
                std::cout << "Check!\n";
        }
    }

    void load(const Board & b)
    {
        board = b;
        root = {};
        current = &root;
        analyze();
    }

    int printHistory(std::ostream & os, Node * node)
    {
        if (node->parent == nullptr)
            // Root node has no move
            return 0;
        board.unapply(node->move);
        int n = printHistory(os, node->parent);
        auto moved = abs(board.content[node->move.from]);
        if (board.trait == 1)
        {
            n += 1;
            os << n << ". ";
        }
        if (moved != Pawn)
            os << get_piece_name(moved);
        os << node->move << ' ';
        board.apply(node->move);
        if (node == current)
            os << '\n';
        return n;
    }

    void printAnalysis(std::ostream & os, const Node * n, int maxDepth, std::string prefix="", bool last=true, bool best=false) const
    {
        os << prefix << (last ? " └─" : " ├─") << (best ? '>' : ' ') << *n << '\n';
        if (n->count <= 1 or maxDepth == 0)
            return;
        prefix += (last ? "    " : " │  ");
        int nChildren = int(n->children.size());
        for (int i = 0; i < nChildren; ++i)
        {
            auto & child = n->children[i];
            if (child.count > 0)
                printAnalysis(os, & child, maxDepth - 1, prefix, i == nChildren - 1, i == n->bestChild);
        }
    }

    void printBestMove(std::ostream & os, const Node * n, std::string prefix="") const
    {
        os << prefix << " └─ " << *n << '\n';
        prefix += "    ";
        for (int i = 0; i < int(n->children.size()); ++i)
        {
            auto & child = n->children[i];
            if (i == n->bestChild)
                printBestMove(os, &child, prefix);
        }
    }

    void play(Node * node)
    {
        board.apply(node->move);
        current = node;
        analyze();
    }

    void unplay()
    {
        assert(current->parent != nullptr);
        board.unapply(current->move);
        current = current->parent;
    }

private:
    void analyze()
    {
        // Expand tree
        while (current->count < strength)
        {
            expand(current);

            if (current->children.empty())
                break;
        }
    }

    int listMovesAndEvaluate(Node * node)
    {
        int mobility = 0;

        int material = board.iterMoves([&](int8_t p, const Move & move, bool valid) {
            // Note: move.taken can also contains a friendly piece to evaluate protection
            // and give more weight to highly movable pieces.
            mobility += sign(p);
            if (valid)
                node->children.push_back({.parent=node, .move=move});
            else if (move.taken == board.trait * King and p * board.trait < 0)
                node->check = true;
        });

        if (node->children.empty())
        {
            if (board.fiftyMovesCount.back() < 50 and node->check)
                return -board.trait * Mate;
            // Draw
            return 0;
        }

        return material + mobility;
    }

    void expand(Node * node)
    {
        if (node->count == 0)
        {
            node->count = 1;
            node->treeEval = node->nodeEval = listMovesAndEvaluate(node);
            return;
        }

        if (node->children.empty())
            return;
        
        int c = re.randIndex(int(node->children.size()));
        Node & child = node->children[c];

        board.apply(child.move);
        expand(& child);
        board.unapply(child.move);
    
        node->count += 1;

        if (c != node->bestChild)
        {
            // Check if the chosen child can become the best one
            if (node->bestChild == -1 or child.treeEval * board.trait >= node->treeEval * board.trait)
            {
                node->treeEval = attenuateEvaluation(child.treeEval);
                node->bestChild = c;
            }

            return;
        }

        // The score of the best child has changed, need to compare all children again
        node->treeEval = -board.trait * Mate;

        for (int o = 0; o < int(node->children.size()); ++o)
        {
            auto & other = node->children[o];

            if (other.treeEval * board.trait >= node->treeEval * board.trait)
            {
                node->treeEval = attenuateEvaluation(other.treeEval);
                node->bestChild = o;
            }
        }
    }

    // Lower intensity of evaluation coming from deep node
    static int attenuateEvaluation(int eval)
    {
        if (eval == 0)
            return 0;
        int s = sign(eval);
        return s * ((s * eval) - 1);
    }
};
